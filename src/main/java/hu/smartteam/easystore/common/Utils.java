package hu.smartteam.easystore.common;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public final class Utils {
    private Utils() {}
    public static <T extends Enum<T>> List<T> StringToEnum(Class<T> enu, Iterable<String> strings) {
        List<T> ret = new LinkedList<>();
        strings.forEach(s -> ret.add(T.valueOf(enu, s.trim().toUpperCase())));
        return ret;
    }

    public static <T extends Enum<T>> List<T> StringToEnum(Class<T> enu, String... strings) {
        return StringToEnum(enu, Arrays.<String>asList(strings));
    }

    @SafeVarargs
    public static <T> T print(T... obj) {
        for (Object o : obj) {
            System.out.print(String.valueOf(o) + " ");
        }
        System.out.println();
        return obj.length>0?obj[0]:null;
    }
}
