/*package hu.smartteam.easystore.common;

import hu.smartteam.easystore.model.Elem;
import hu.smartteam.easystore.repositories.CompartmentRepository;
import hu.smartteam.easystore.repositories.ElemRepository;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

@Component
public class UniqueSerialGenerator {
@Autowired private CompartmentRepository compartmentRepository;
@Autowired private ElemRepository elemRepository;

private String prev = null;

public String next() {
if (prev==null) init();
}

private void init() {
try {
prev = Long.toString(Long.parseLong(elemRepository.findAll(new Sort(Sort.Direction.DESC, "serial"))
.get(0).getSerial()) + 1l);
}
catch (NumberFormatException e) {
String uuid;
while (true) {
uuid = UUID.randomUUID().toString().substring(0, 3);
if (elemRepository.findOne(Elem.exampleBuider().serial(uuid).build())==null) {
prev = uuid;
break;
}
}
}
catch (RuntimeException e) {
e.printStackTrace();
if (elemRepository.findOne(Elem.exampleBuider().serial("0").build())==null) prev = "0";
else throw e;
}
}
}*/