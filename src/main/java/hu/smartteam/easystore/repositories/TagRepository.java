package hu.smartteam.easystore.repositories;

import hu.smartteam.easystore.model.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TagRepository extends JpaRepository<Tag, Long> {
    public Tag findByText(String text);
}