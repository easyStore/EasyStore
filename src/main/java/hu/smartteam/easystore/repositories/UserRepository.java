package hu.smartteam.easystore.repositories;

import hu.smartteam.easystore.model.User;
import javax.annotation.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long>{
    @Nullable
    User findByUserName(String userName);
}
