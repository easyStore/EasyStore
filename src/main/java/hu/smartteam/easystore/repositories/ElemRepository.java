package hu.smartteam.easystore.repositories;

import hu.smartteam.easystore.model.Elem;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ElemRepository extends JpaRepository<Elem, Long>{
    List<Elem> findByDateAddedBetween(LocalDateTime start, LocalDateTime end);
}
