package hu.smartteam.easystore.repositories;

import hu.smartteam.easystore.model.Compartment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompartmentRepository extends JpaRepository<Compartment, Long>{
}
