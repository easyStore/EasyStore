package hu.smartteam.easystore.model;

import java.io.Serializable;
import java.util.Objects;
import java.util.Properties;
import javax.persistence.*;

@Entity
@SuppressWarnings("serial")
@Table(name = "UserTable")
public class User implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Long id;

    @Column(name = "userName", nullable = false, unique = true)
    private String userName;

    @Column(name = "passwd")
    private String password;

    @Lob
    @Column(name = "infos")
    private Properties infos;

    @Enumerated(EnumType.STRING)
    @Column(name = "auth")
    private Authority authority;

    @Column(name = "enabl")
    private Boolean enabled;

    protected User() {} //For serialization only

    public User(String userName, String password, Authority authority) {
        this.userName = userName;
        this.password = password;
        this.authority = authority;
        enabled = true;
    }
    public Long getId() { return id; }
    public String getUserName() { return userName; }
    public String getPassword() { return password; }
    public Properties getInfos() { return infos==null ? new Properties() : infos; }
    public Authority getAuthority() { return authority; }
    public boolean getEnabled() { return enabled; }
    public void setPassword(String password) { this.password = password; }
    @SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
    public User setInfos(Properties infos) {
        this.infos = infos;
        return this;
    }
    public void setAuthority(Authority authority) { this.authority = authority; }
    public void setEnabled(boolean enabled) { this.enabled = enabled; }
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.userName);
        return hash;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final User other = (User) obj;
        if (!Objects.equals(this.userName, other.userName)) return false;
        return Objects.equals(this.id, other.id);
    }
    @Override
    public String toString() {
        return "User{" + "id=" + id + ", userName=" + userName + ", infos=" + infos + '}' + ", authority=" + authority;
    }
}
