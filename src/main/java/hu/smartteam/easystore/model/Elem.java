package hu.smartteam.easystore.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import javax.persistence.*;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;

@Entity
@Table(name = "element")
@SuppressWarnings("serial")
public class Elem implements Serializable {

    @Column(name = "name")
    private String name;

    @Column(name = "value_col")
    private Double value;

    @Column(name = "description")
    private String desc;

    @Column(name = "status")
    private String status;

    @Column(name = "serial")
    private String serial;

    @Column(name = "dateAdded")
    private LocalDateTime dateAdded;

    @ManyToOne
    @JoinColumn(name = "elements")
    private Compartment compartment;

    @GeneratedValue
    @Column(name = "id")
    @Id
    private Long id;

    @Lob
    @Column(name = "infos")
    private Properties infos;

    @Lob
    @Column(name = "pictures")
    private ArrayList<String> pictures;

    @ManyToMany(mappedBy = "tag_elements")
    private Set<Tag> tags;

    private Elem(Void nul){} //For prototype creation
    protected Elem(){ //For serialization only
        this.dateAdded = LocalDateTime.now();
    }
    public Elem(String name, String serial, Compartment compartment) {
        this();
        this.name = name;
        this.serial = serial;
        this.compartment = compartment;
    }
    public Elem(String name, Double value, String desc, String status, String serial, Compartment compartment, List<String> pictures, Properties infos, Set<Tag> tags) {
        this(name, serial, compartment);
        this.value = value;
        this.desc = desc;
        this.status = status;
        this.pictures = new ArrayList<>(pictures==null?Collections.emptyList():pictures);
        this.infos = infos;
        this.tags = tags;
    }

    public static ExampleBuider exampleBuider() {
        return new ExampleBuider();
    }
    public static ExampleBuider exampleBuider(ExampleMatcher.StringMatcher stringMatcher) {
        return new ExampleBuider(stringMatcher);
    }
    public static ElemBuilder elemBuilder() {
        return new ElemBuilder();
    }

    public String getName() { return name; }
    public Double getValue() { return value; }
    public String getDesc() { return desc; }
    public String getStatus() { return status; }
    public String getSerial() { return serial; }
    public LocalDateTime getDateAdded() { return dateAdded; }
    public Compartment getCompartment() { return compartment; }
    public Long getId() { return id; }
    @SuppressWarnings("ReturnOfCollectionOrArrayField")
    public Properties getInfos() { return infos==null?new Properties():infos; }
    @SuppressWarnings("ReturnOfCollectionOrArrayField")
    public List<String> getPictures() { return pictures==null?new ArrayList<>(3):pictures; }
    public Set<Tag> getTags() { return tags==null?new HashSet<>(3):tags; }
    public void setName(String name) { this.name = name; }
    public void setValue(Double value) { this.value = value; }
    public void setDesc(String desc) { this.desc = desc; }
    public void setStatus(String status) { this.status = status; }
    public void setSerial(String serial) { this.serial = serial; }
    public void setCompartment(Compartment compartment) { this.compartment = compartment; }
    @SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
    public void setInfos(Properties infos) { this.infos = infos; }
    public void setPictures(List<String> pictures) { this.pictures = new ArrayList<>(pictures); }
    public void setTags(Set<Tag> tags) { this.tags = new HashSet<>(tags); }
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final Elem other = (Elem) obj;
        if (!Objects.equals(this.name, other.name)) return false;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return "Elem{" + "name=" + name + ", value=" + value + ", desc=" + desc + ", status=" + status + ", serial=" + serial + ", compartment=" + compartment + ", tags=" + tags + '}';
    }

    public void overwrite(Elem newElem) {
        this.name = newElem.name;
        this.compartment = newElem.compartment;
        this.value = newElem.value;
        this.desc = newElem.desc;
        this.status = newElem.status;
        this.infos = newElem.infos;
        this.pictures = newElem.pictures;
        this.tags = newElem.tags;
    }

    public static class ExampleBuider {
        private final Elem prototype;
        private final ExampleMatcher.StringMatcher stringMatcher;
        private ExampleBuider() {
            this(ExampleMatcher.StringMatcher.DEFAULT);
        }
        private ExampleBuider(ExampleMatcher.StringMatcher stringMatcher) {
            prototype = new Elem(null);
            this.stringMatcher = stringMatcher;
        }
        public ExampleBuider name(String name) { prototype.name = name; return this; }
        public ExampleBuider value(double value) { prototype.value = value; return this; }
        public ExampleBuider desc(String desc) { prototype.desc = desc; return this; }
        public ExampleBuider status(String status) { prototype.status = status; return this; }
        public ExampleBuider serial(String serial) { prototype.serial = serial; return this; }
        public ExampleBuider compartment(Compartment compartment) { prototype.compartment = compartment; return this; }
        public ExampleBuider id(long id) { prototype.id = id; return this; }
        public ExampleBuider tags(Set<Tag> tags) { prototype.tags = tags; return this; }
        public Example<Elem> build() {
            if (prototype.infos==null) return Example.of(prototype, ExampleMatcher.matching()
                                                                                  .withIgnorePaths("infos", "pictures", "compartment.name", "compartment.room", "compartment.shelf", "compartment.infos", "compartment.pictures", "compartment.id", "compartment.elements", "compartment.tags")
                                                                                  .withStringMatcher(stringMatcher));
            return Example.of(prototype);
        }
    }

    public static class ElemBuilder {
        private final Elem temporary;
        private ElemBuilder() {
            temporary = new Elem(null);
        }
        public ElemBuilder name(String name) { temporary.name = name; return this; }
        public ElemBuilder value(double value) { temporary.value = value; return this; }
        public ElemBuilder desc(String desc) { temporary.desc = desc; return this; }
        public ElemBuilder status(String status) { temporary.status = status; return this; }
        public ElemBuilder serial(String serial) { temporary.serial = serial; return this; }
        public ElemBuilder compartment(Compartment compartment) { temporary.compartment = compartment; return this; }
        public ElemBuilder infos(Properties infos) { temporary.infos = infos; return this; }
        public ElemBuilder pictures(List<String> pictures) { temporary.pictures = new ArrayList<>(pictures); return this; }
        public ElemBuilder tags(Set<Tag> tags) { temporary.tags = new HashSet<>(tags); return this; }
        public Elem build() {
            return new Elem(temporary.name, temporary.value, temporary.desc, temporary.status, temporary.serial, temporary.compartment, temporary.pictures, temporary.infos, temporary.tags);
        }
    }
}
