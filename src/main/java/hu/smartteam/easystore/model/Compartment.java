package hu.smartteam.easystore.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import javax.persistence.*;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;

@Entity
@Table(name = "Compartment")
@SuppressWarnings("serial")
public class Compartment implements Serializable {
    @Column(name = "serial", unique = true)
    private String serial;

    @Column(name = "name")
    private String name;

    @Column(name = "room")
    private String room;

    @Column(name = "shelf")
    private String shelf;

    @Lob
    @Column(name = "infos")
    private Properties infos;

    @Lob
    @Column(name = "pictures")
    private ArrayList<String> pictures;

    @GeneratedValue
    @Column(name = "id")
    @Id
    private Long id;

    @OneToMany(mappedBy = "compartment")
    @Column(name = "elements")
    private Set<Elem> elements;

    @ManyToMany(mappedBy = "tag_compartments")
    private Set<Tag> tags;

    private Compartment(Void nul){} //For prototype creation
    protected Compartment(){} //For serialization only
    public Compartment(String serial, String name, String room, String shelf, List<String> pictures, Properties infos, Set<Tag> tags) {
        if (serial==null) throw new IllegalArgumentException("Serial cannot be null");
        this.serial = serial;
        this.name = name;
        this.room = room;
        this.shelf = shelf;
        this.pictures = new ArrayList<>(pictures==null?Collections.emptyList():pictures);
        this.infos = infos;
        this.tags = tags;
    }
    public Compartment(String serial, String name) {
        this(serial, name, "", "", null, null, null);
    }
    public static ExampleBuilder exampleBuilder() {
        return new ExampleBuilder();
    }
    public static ExampleBuilder exampleBuilder(ExampleMatcher.StringMatcher stringMatcher) {
        return new ExampleBuilder(stringMatcher);
    }
    public static CompartmentBuilder compartmentBuilder() {
        return new CompartmentBuilder();
    }
    public String getSerial() { return serial; }
    public String getName() { return name; }
    public String getRoom() { return room; }
    public String getShelf() { return shelf; }
    @SuppressWarnings("ReturnOfCollectionOrArrayField")
    public Properties getInfos() { return infos==null?new Properties():infos; }
    @SuppressWarnings("ReturnOfCollectionOrArrayField")
    public List<String> getPictures() { return pictures==null?new ArrayList<>(3):pictures; }
    public Long getId() { return id; }
    @SuppressWarnings("ReturnOfCollectionOrArrayField")
    public Set<Elem> getElements() { return elements; }
    @SuppressWarnings("ReturnOfCollectionOrArrayField")
    public Set<Tag> getTags() { return tags==null?new HashSet<>(3):tags; }
    public void setSerial(String serial) { this.serial = serial; }
    public void setName(String name) { this.name = name; }
    public void setRoom(String room) { this.room = room; }
    public void setShelf(String shelf) { this.shelf = shelf; }
    @SuppressWarnings("AssignmentToCollectionOrArrayFieldFromParameter")
    public void setInfos(Properties infos) { this.infos = infos; }
    public void setPictures(List<String> pictures) { this.pictures = new ArrayList<>(pictures); }
    public void setTags(Set<Tag> tags) { this.tags = new HashSet<>(tags); }
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.id);
        return hash;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final Compartment other = (Compartment) obj;
        if (!Objects.equals(this.name, other.name)) return false;
        return Objects.equals(this.id, other.id);
    }
    @Override
    public String toString() {
        return "Compartment{" + "serial=" + serial + ", name=" + name + ", room=" + room + ", shelf=" + shelf + ", infos=" + infos + ", tags=" + tags + '}';
    }

    public void overwrite(Compartment newComp) {
        this.name = newComp.name;
        this.room = newComp.room;
        this.shelf = newComp.shelf;
        this.pictures = newComp.pictures;
        this.infos = newComp.infos;
        this.tags = newComp.tags;
    }

    public static class ExampleBuilder {
        private final Compartment prototype;
        private final ExampleMatcher.StringMatcher stringMatcher;
        private ExampleBuilder() {
            this(ExampleMatcher.StringMatcher.DEFAULT);
        }
        private ExampleBuilder(ExampleMatcher.StringMatcher stringMatcher) {
            prototype = new Compartment(null);
            this.stringMatcher = stringMatcher;
        }
        public ExampleBuilder serial(String serial) { prototype.serial = serial; return this; }
        public ExampleBuilder name(String name) { prototype.name = name; return this; }
        public ExampleBuilder room(String room) { prototype.room = room; return this; }
        public ExampleBuilder shelf(String shelf) { prototype.shelf = shelf; return this; }
        public ExampleBuilder id(long id) { prototype.id = id; return this; }
        public ExampleBuilder elements(Set<Elem> elements) { prototype.elements = elements; return this; }
        public ExampleBuilder tags(Set<Tag> tags) { prototype.tags = tags; return this; }
        public Example<Compartment> build() {
            if (prototype.infos==null)
                return Example.of(prototype, ExampleMatcher.matching()
                                                           .withIgnorePaths("infos", "pictures")
                                                           .withStringMatcher(stringMatcher)
                                                           .withMatcher("tags", match -> match.transform(source -> ((Iterable) source).iterator().next())));
            return Example.of(prototype);
        }
    }

    public static class CompartmentBuilder {
        private final Compartment temporary;
        private CompartmentBuilder(){
            temporary = new Compartment();
        }
        public CompartmentBuilder serial(String serial) { temporary.serial = serial; return this; }
        public CompartmentBuilder name(String name) { temporary.name = name; return this; }
        public CompartmentBuilder room(String room) { temporary.room = room; return this; }
        public CompartmentBuilder shelf(String shelf) { temporary.shelf = shelf; return this; }
        public CompartmentBuilder pictures(List<String> pictures) { temporary.pictures = new ArrayList<>(pictures); return this; }
        public CompartmentBuilder infos(Properties infos) { temporary.infos = infos; return this; }
        public CompartmentBuilder tags(Set<Tag> tags) { temporary.tags = tags; return this; }
        public Compartment build() {
            return new Compartment(temporary.serial, temporary.name, temporary.room, temporary.shelf, temporary.pictures, temporary.infos, temporary.tags);
        }
    }
}
