package hu.smartteam.easystore.model;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
@SuppressWarnings("serial")
public class Tag implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
        name = "tag_compartment",
        joinColumns = @JoinColumn(name = "tag_id"),
        inverseJoinColumns = @JoinColumn(name = "compartment_id"))
    private Set<Compartment> tag_compartments;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
        name = "tag_element",
        joinColumns = @JoinColumn(name = "tag_id"),
        inverseJoinColumns = @JoinColumn(name = "elem_id"))
    private Set<Elem> tag_elements;

    @Column(unique = true)
    private String text;

    protected Tag() {}
    public Tag(Set<Compartment> compartments, Set<Elem> elements, String text) {
        this.tag_compartments = compartments;
        this.tag_elements = elements;
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @SuppressWarnings("ReturnOfCollectionOrArrayField")
    public Set<Compartment> getCompartments() {
        return tag_compartments;
    }

    @SuppressWarnings("ReturnOfCollectionOrArrayField")
    public Set<Elem> getElements() {
        return tag_elements;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        final Tag other = (Tag) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return "Tag{" + text + '}';
    }
}