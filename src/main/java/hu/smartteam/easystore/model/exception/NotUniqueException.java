package hu.smartteam.easystore.model.exception;

@SuppressWarnings("serial")
public class NotUniqueException extends Exception {
    public NotUniqueException() {
    }

    public NotUniqueException(String message) {
        super(message);
    }

    public NotUniqueException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotUniqueException(Throwable cause) {
        super(cause);
    }

}
