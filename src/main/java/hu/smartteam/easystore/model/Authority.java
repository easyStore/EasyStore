package hu.smartteam.easystore.model;

public enum Authority {
    ADMIN, USER, GUEST
}
