package hu.smartteam.easystore.service;

import hu.smartteam.easystore.model.User;
import hu.smartteam.easystore.model.exception.NotUniqueException;
import java.util.List;

public interface UserService {

    public User addUser(User user) throws IllegalArgumentException, NotUniqueException;
    public boolean deleteUser(Long id);
    public User get(Long id);
    public User get(String username);
    public List<User> getAll();
}
