package hu.smartteam.easystore.service;

import hu.smartteam.easystore.model.Elem;
import java.util.List;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;

public interface ElemService {

    public Elem save(Elem elem);
    public void delete(Elem elem);
    public Elem findOne(Example<Elem> example);
    public List<Elem> find(Example<Elem> example);
    public Elem findBySerial(String serial, String compSerial);
    public Page<Elem> findAll(int page, int pageSize);
    public List<Elem> getAll();
    public String generateSerial(String compSerial);
}
