package hu.smartteam.easystore.service;

import hu.smartteam.easystore.model.Compartment;
import java.util.List;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;

public interface CompartmentService {

    public Compartment save(Compartment compartment);
    public void delete(Compartment compartment);
    public Compartment findOne(Example<Compartment> example);
    public List<Compartment> find(Example<Compartment> example);
    public Compartment findBySerial(String serial);
    public Page<Compartment> findAll(int page, int pageSize);
    public List<Compartment> getAll();
}
