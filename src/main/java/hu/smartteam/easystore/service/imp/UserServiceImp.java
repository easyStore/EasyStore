package hu.smartteam.easystore.service.imp;

import hu.smartteam.easystore.model.User;
import hu.smartteam.easystore.model.exception.NotUniqueException;
import hu.smartteam.easystore.repositories.UserRepository;
import hu.smartteam.easystore.service.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static hu.smartteam.easystore.common.Logger.LOGGER;

@Service
public class UserServiceImp implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImp(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User addUser(User user) throws IllegalArgumentException, NotUniqueException {
        if (user==null)
            throw (IllegalArgumentException) LOGGER.log(new IllegalArgumentException("User is null"));
        if (user.getUserName()==null)
            throw (IllegalArgumentException) LOGGER.log(new IllegalArgumentException("Username is null"));
        if (user.getPassword()==null)
            throw (IllegalArgumentException) LOGGER.log(new IllegalArgumentException("Password is null"));
        User prev = userRepository.findByUserName(user.getUserName());
        if (prev!=null) {
            if (prev.getEnabled())
                throw (NotUniqueException) LOGGER.log(new NotUniqueException("Username is already in use."));
            prev.setEnabled(true);
            prev.setInfos(user.getInfos());
            prev.setPassword(user.getPassword());
            prev.setAuthority(user.getAuthority());
            return userRepository.save(prev);
        }
        return userRepository.save(user);
    }

    @Override
    public boolean deleteUser(Long id) {
        if (id==null) return false;
        User prev = userRepository.findOne(id);
        if (prev==null) return false;
        prev.setEnabled(false);
        userRepository.save(prev);
        return true;
    }

    @Override
    public User get(Long id) {
        return userRepository.findOne(id);
    }

    @Override
    public User get(String username) {
        return userRepository.findByUserName(username);
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }
}