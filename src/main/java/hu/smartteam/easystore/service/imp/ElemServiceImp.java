package hu.smartteam.easystore.service.imp;

import hu.smartteam.easystore.model.Compartment;
import hu.smartteam.easystore.model.Elem;
import hu.smartteam.easystore.repositories.ElemRepository;
import hu.smartteam.easystore.service.CompartmentService;
import hu.smartteam.easystore.service.ElemService;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class ElemServiceImp implements ElemService {
    private final ElemRepository elemRepository;
    private final CompartmentService compartmentService;

    @Autowired
    public ElemServiceImp(ElemRepository elemRepository, CompartmentService compartmentService) {
        this.elemRepository = elemRepository;
        this.compartmentService = compartmentService;
    }

    @Override
    public Elem save(Elem elem) {
        if (elem == null) throw new IllegalArgumentException("Elem is null.");
        Elem existing = findBySerial(elem.getSerial(), elem.getCompartment().getSerial());
        if (existing != null) {
            existing.overwrite(elem);
            return elemRepository.save(existing);
        }
        return elemRepository.save(elem);
    }

    @Override
    public void delete(Elem elem) {
        elemRepository.delete(findBySerial(elem.getSerial(), elem.getCompartment().getSerial()));
    }

    @Override
    public Elem findOne(Example<Elem> example) {
        return elemRepository.findOne(example);
    }

    @Override
    public List<Elem> find(Example<Elem> example) {
        return elemRepository.findAll(example);
    }

    @Override
    public Elem findBySerial(String serial, String compSerial) {
        return elemRepository.findOne(Elem.exampleBuider()
            .serial(serial)
            .compartment(new Compartment(compSerial, null))
            .build());
    }

    @Override
    public Page<Elem> findAll(int page, int pageSize) {
        Pageable pageable = new PageRequest(page, pageSize, new Sort(Sort.Direction.DESC, "serial"));
        return elemRepository.findAll(pageable);
    }

    @Override
    public List<Elem> getAll() {
        return elemRepository.findAll();
    }

    @Override
    public String generateSerial(String compSerial) {
        try {
            return Long.toString(Long.parseLong(elemRepository.findAll(
                      Elem.exampleBuider().compartment(compartmentService.findBySerial(compSerial)).build()
                    , new Sort(Sort.Direction.DESC, "serial"))
                .get(0).getSerial()) + 1l);
        }
        catch (IndexOutOfBoundsException e) {
            return "1";
        }
        catch (NumberFormatException e) {
            String uuid;
            while (true) {
                uuid = UUID.randomUUID().toString().substring(0, 3);
                if (elemRepository.findOne(Elem.exampleBuider().serial(uuid).build())==null) return uuid;
            }
        }
        catch (RuntimeException e) {
            e.printStackTrace();
            if (elemRepository.findOne(Elem.exampleBuider().serial("0").build())==null) return "0";
            else throw e;
        }
    }
}
