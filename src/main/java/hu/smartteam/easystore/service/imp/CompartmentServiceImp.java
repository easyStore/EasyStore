package hu.smartteam.easystore.service.imp;

import hu.smartteam.easystore.model.Compartment;
import hu.smartteam.easystore.repositories.CompartmentRepository;
import hu.smartteam.easystore.service.CompartmentService;
import java.util.List;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class CompartmentServiceImp implements CompartmentService {
    private final CompartmentRepository compartmentRepository;

    public CompartmentServiceImp(CompartmentRepository compartmentRepository) {
        this.compartmentRepository = compartmentRepository;
    }

    @Override
    public Compartment save(Compartment compartment) {
        if (compartment==null) throw new IllegalArgumentException("Compartment is null.");
        Compartment existing = findBySerial(compartment.getSerial());
        if (existing!=null) {
            existing.overwrite(compartment);
            return compartmentRepository.save(existing);
        }
        return compartmentRepository.save(compartment);
    }

    @Override
    public void delete(Compartment compartment) {
        compartmentRepository.delete(findBySerial(compartment.getSerial()));
    }

    @Override
    public Compartment findOne(Example<Compartment> example) {
        return compartmentRepository.findOne(example);
    }

    @Override
    public List<Compartment> find(Example<Compartment> example) {
        return compartmentRepository.findAll(example);
    }

    @Override
    public Compartment findBySerial(String serial) {
        return findOne(Compartment.exampleBuilder().serial(serial).build());
    }

    @Override
    public Page<Compartment> findAll(int page, int pageSize) {
        Pageable pageable = new PageRequest(page, pageSize, new Sort(Sort.Direction.DESC, "serial"));
        return compartmentRepository.findAll(pageable);
    }

    @Override
    public List<Compartment> getAll() {
        return compartmentRepository.findAll();
    }
}
