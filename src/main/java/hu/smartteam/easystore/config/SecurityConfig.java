/*package hu.smartteam.easystore.config;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

@Autowired private DataSource dataSource;

@Override
protected void configure(AuthenticationManagerBuilder auth) throws Exception {
auth.jdbcAuthentication()
.dataSource(dataSource)
.usersByUsernameQuery("select user_name, passwd, enabl from user_table where user_name = ?")
.authoritiesByUsernameQuery("select user_name, auth from user_table where user_name = ?")
.passwordEncoder(new BCryptPasswordEncoder());
}

@Override
protected void configure(HttpSecurity sec) throws Exception {
sec.csrf().disable().authorizeRequests()
.antMatchers("/**").hasAuthority("ADMIN")
.anyRequest().authenticated()
.and()
.formLogin().loginPage("/login").permitAll().and().logout().permitAll();
}
}*/