package hu.smartteam.easystore.controller;
import hu.smartteam.easystore.model.Compartment;
import hu.smartteam.easystore.model.Elem;
import hu.smartteam.easystore.model.Tag;
import hu.smartteam.easystore.repositories.TagRepository;
import hu.smartteam.easystore.service.CompartmentService;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@SuppressWarnings({"AssignmentToCollectionOrArrayFieldFromParameter","ReturnOfCollectionOrArrayField"})
public class ObjectTransferHandle {

    private final CompartmentService compartmentService;
    private final TagRepository tagRepository;

    @Value("${images-address-prefix}")
    private String imagesAddressPrefix;

    @Autowired
    ObjectTransferHandle(CompartmentService compartmentService, TagRepository tagRepository) {
        this.compartmentService = compartmentService;
        this.tagRepository = tagRepository;
    }

    private String imageFileToBase64(String filename) throws IOException {
        return Base64.getEncoder().encodeToString(Files.readAllBytes(Paths.get(imagesAddressPrefix + filename)));
    }
    @SuppressWarnings("AssignmentToMethodParameter")
    private void base64ToImageFile(String base64, String filename) throws IOException {
        base64 = base64.replaceAll("\\s", "");
        Path parentDir = Paths.get(imagesAddressPrefix + filename).getParent();
        if (!Files.exists(parentDir)) Files.createDirectories(parentDir);
        Files.write(Paths.get(imagesAddressPrefix + filename), Base64.getDecoder().decode(base64));
    }
    private List<String> imagesToBase64(List<String> filenames) {
        List<String> imagesBase64 = new ArrayList<>(3);
        filenames.forEach((pic) -> {
                try {
                    imagesBase64.add(imageFileToBase64(pic));
                }
                catch (IOException ex) {
                    ex.printStackTrace();
                }
            });
        return imagesBase64;
    }
    private List<String> base64ToImages(CompartmentTransfer dto) {
        if (dto.getImagesBase64().isEmpty()) return Collections.emptyList();
        String filename = "c_" + dto.getSerial();
        List<String> filenames = new ArrayList<>(3);
        int increment = 0;
        for (String base64 : dto.getImagesBase64()) {
            try {
                base64ToImageFile(base64, filename + "_" + increment + ".jpg");
                filenames.add(filename + "_" + increment + ".jpg");
                increment++;
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        return filenames;
    }
    private List<String> base64ToImages(ElemTransfer dto) {
        if (dto.getImagesBase64().isEmpty()) return Collections.emptyList();
        String filename = "e_" + dto.getCompartmentSerial() + "_" + dto.getSerial();
        List<String> filenames = new ArrayList<>(3);
        int increment = 0;
        for (String base64 : dto.getImagesBase64()) {
            try {
                base64ToImageFile(base64, filename + "_" + increment + ".jpg");
                filenames.add(filename + "_" + increment + ".jpg");
                increment++;
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        return filenames;
    }


    public CompartmentTransfer conv(Compartment entity) {
        if (entity==null) return null;
        return new CompartmentTransfer(){{
            setSerial(entity.getSerial());
            setName(entity.getName());
            setRoom(entity.getRoom());
            setShelf(entity.getShelf());
            setInfos(entity.getInfos());
            setImagesBase64(imagesToBase64(entity.getPictures()));
            setTags(entity.getTags().stream().map(e -> e.getText()).collect(Collectors.toSet()));
        }};
    }
    public ElemTransfer conv(Elem entity){
        if (entity==null) return null;
        return new ElemTransfer(){{
            setSerial(entity.getSerial());
            setName(entity.getName());
            setStatus(entity.getStatus());
            setDesc(entity.getDesc());
            setValue(entity.getValue());
            setInfos(entity.getInfos());
            setCompartmentSerial(entity.getCompartment().getSerial());
            setImagesBase64(imagesToBase64(entity.getPictures()));
            setTags(entity.getTags().stream().map(e -> e.getText()).collect(Collectors.toSet()));
        }};
    }
    public Compartment conv(CompartmentTransfer dto) {
        Set<Tag> tags;
        if (dto.tags==null) tags = Collections.emptySet();
        else tags = dto.tags.stream().map(dt -> tagRepository.findByText(dt)).collect(Collectors.toSet());
        return new Compartment(dto.serial, dto.name, dto.room, dto.shelf, base64ToImages(dto), dto.infos, tags);
    }
    public Elem conv(ElemTransfer dto) throws IllegalArgumentException {
        Set<Tag> tags;
        if (dto.tags==null) tags = Collections.emptySet();
        else tags = dto.tags.stream().map(dt -> tagRepository.findByText(dt)).collect(Collectors.toSet());
        Compartment compartment = compartmentService.findBySerial(dto.compartmentSerial);
        if (compartment == null) throw new IllegalArgumentException("Compartment with the given serial does not exist.");
        return new Elem(dto.name, dto.value, dto.desc, dto.status, dto.serial, compartment, base64ToImages(dto), dto.infos, tags);
    }

    public static class CompartmentTransfer implements Dto{
        @NotEmpty
        @Length(max = 256)
        private String serial;
        @NotEmpty
        private String name;
        private String room;
        private String shelf;
        private List<String> imagesBase64;
        private Properties infos;
        private Set<String> tags;
        public CompartmentTransfer(){}
        public void setSerial(String serial) { this.serial = serial; }
        public void setName(String name) { this.name = name; }
        public void setRoom(String room) { this.room = room; }
        public void setShelf(String shelf) { this.shelf = shelf; }
        public void setInfos(Properties infos) { this.infos = infos; }
        public void setImagesBase64(List<String> imagesBase64) { this.imagesBase64 = imagesBase64; }
        public void setTags(Set<String> tags) { this.tags = tags; }
        @Override
        public String getSerial() { return serial; }
        public String getName() { return name; }
        public String getRoom() { return room; }
        public String getShelf() { return shelf; }
        public Properties getInfos() { return infos; }
        @Override
        public List<String> getImagesBase64() { return imagesBase64; }
        public Set<String> getTags() { return tags; }
        @Override
        public String toString() {
            return "CompartmentTransfer{" + "serial=" + serial + ", name=" + name + ", room=" + room + ", shelf=" + shelf + ", infos=" + infos + ", noOfPics=" + imagesBase64.size() + ", tags=" + tags + '}';
        }
    }
    public static class ElemTransfer implements Dto {
        @NotEmpty
        private String name;
        private Double value;
        private String desc;
        private String status;
        @NotEmpty
        @Length(max = 256)
        private String serial;
        private String compartmentSerial;
        private Properties infos;
        private List<String> imagesBase64;
        private Set<String> tags;
        public ElemTransfer(){}
        public void setName(String name) { this.name = name; }
        public void setValue(Double value) { this.value = value; }
        public void setDesc(String desc) { this.desc = desc; }
        public void setStatus(String status) { this.status = status; }
        public void setSerial(String serial) { this.serial = serial; }
        public void setCompartmentSerial(String compartmentSerial) { this.compartmentSerial = compartmentSerial; }
        public void setInfos(Properties infos) { this.infos = infos; }
        public void setImagesBase64(List<String> imagesBase64) { this.imagesBase64 = imagesBase64; }
        public void setTags(Set<String> tags) { this.tags = tags; }
        public String getName() { return name; }
        public Double getValue() { return value; }
        public String getDesc() { return desc; }
        public String getStatus() { return status; }
        @Override
        public String getSerial() { return serial; }
        public String getCompartmentSerial() { return compartmentSerial; }
        public Properties getInfos() { return infos; }
        @Override
        public List<String> getImagesBase64() { return imagesBase64; }
        public Set<String> getTags() { return tags; }
        @Override
        public String toString() {
            return "ElemTransfer{" + "name=" + name + ", value=" + value + ", desc=" + desc + ", status=" + status + ",  serial=" + serial + ", compartmentSerial=" + compartmentSerial + ", infos=" + infos + ", noOfPics=" + imagesBase64.size() + ", tags=" + tags + '}';
        }
    }
    public interface Dto {
        String getSerial();
        List<String> getImagesBase64();
    }
}
