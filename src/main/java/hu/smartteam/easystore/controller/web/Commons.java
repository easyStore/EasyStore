package hu.smartteam.easystore.controller.web;

import hu.smartteam.easystore.controller.ValidationError;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class Commons {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    @ResponseBody
    public List<ValidationError> processValidationError(MethodArgumentNotValidException e) {
        return e.getBindingResult().getFieldErrors().stream()
            .map(fieldError -> new ValidationError(fieldError.getField(), fieldError.getDefaultMessage()))
            .collect(Collectors.toList());
    }

    static void listDeleteRest(List<?> list, int deleteFrom) {
        if (deleteFrom >= list.size()) return;
        list.subList(deleteFrom, list.size()).clear();
    }

    static ResponseEntity<String> responseEntity(String s, HttpStatus status) {
        return new ResponseEntity<>("[\"" + s + "\"]", status);
    }

    static ResponseEntity<String> validatedResponseEntity() {
        return new ResponseEntity<>("[{\"message\":\"Success\"}]", HttpStatus.OK);
    }

}