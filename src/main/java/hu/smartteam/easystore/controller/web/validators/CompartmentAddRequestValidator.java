package hu.smartteam.easystore.controller.web.validators;

import hu.smartteam.easystore.controller.ObjectTransferHandle;
import hu.smartteam.easystore.service.CompartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class CompartmentAddRequestValidator implements Validator {

    private final CompartmentService compartmentService;

    @Autowired
    public CompartmentAddRequestValidator(CompartmentService compartmentService) {
        this.compartmentService = compartmentService;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return ObjectTransferHandle.CompartmentTransfer.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ObjectTransferHandle.CompartmentTransfer dto = (ObjectTransferHandle.CompartmentTransfer) target;
        if (compartmentService.findBySerial(dto.getSerial())!=null) {
            errors.rejectValue("serial", "alreadyExists", "already exists");
        }
    }

}