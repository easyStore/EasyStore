package hu.smartteam.easystore.controller.web;

import hu.smartteam.easystore.controller.ObjectTransferHandle;
import hu.smartteam.easystore.controller.ObjectTransferHandle.ElemTransfer;
import hu.smartteam.easystore.controller.web.validators.ElementAddRequestValidator;
import hu.smartteam.easystore.model.Elem;
import hu.smartteam.easystore.service.CompartmentService;
import hu.smartteam.easystore.service.ElemService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import static hu.smartteam.easystore.common.Utils.print;

@Controller
@RequestMapping("/elem")
public class ElemController {

    private final ObjectTransferHandle oth;
    private final ElemService elemService;
    private final CompartmentService compartmentService;
    private final ElementAddRequestValidator addVal;

    @Autowired
    ElemController(ObjectTransferHandle oth,
                   ElemService elemService,
                   CompartmentService compartmentService,
                   ElementAddRequestValidator addVal) {
        this.oth = oth;
        this.elemService = elemService;
        this.compartmentService = compartmentService;
        this.addVal = addVal;
    }

    @InitBinder("elemTransferBugfix")
    public void setupBinder(WebDataBinder binder) {
        binder.addValidators(addVal);
    }

    @PostMapping("/add")
    public ResponseEntity<String> addElem(@Valid @RequestBody ElemTransferBugfix dto) {
        return saveElem(dto);
    }

    @PostMapping("/save")
    public ResponseEntity<String> saveElem(@Valid @RequestBody ObjectTransferHandle.ElemTransfer dto) {
        System.out.println(dto);
        try {
            Elem elem = oth.conv(dto);
            elemService.save(elem);

            return Commons.validatedResponseEntity();
        }
        catch(IllegalArgumentException e) {
            return Commons.responseEntity("Error: " + e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        catch(Exception e) {
            e.printStackTrace();
            return Commons.responseEntity("Unexpected error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/delete")
    public ResponseEntity<String> deleteElement(@RequestParam String compSerial, @RequestParam String serial) {
        print("deleting");
        try {
            elemService.delete(elemService.findBySerial(serial, compSerial));
            return Commons.responseEntity("Success", HttpStatus.OK);
        }
        catch(Exception e) {
            e.printStackTrace();
            return Commons.responseEntity("Unexpected error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("")
    @ResponseBody
    public ObjectTransferHandle.ElemTransfer getElem(@RequestParam String serial, @RequestParam String compSerial) {
        return oth.conv(elemService.findBySerial(serial, compSerial));
    }

    @GetMapping("/all")
    @ResponseBody
    public List<ObjectTransferHandle.ElemTransfer> getAllElem(@RequestParam String compSerial,
                                                              @RequestParam Integer page,
                                                              @RequestParam Integer pageSize,
                                                              @RequestParam(required = false) String requestImages) {
        List<ObjectTransferHandle.ElemTransfer> elems = new ArrayList<>(pageSize);
        elemService.find(Elem.exampleBuider()
            .compartment(compartmentService.findBySerial(compSerial))
            .build()).forEach((el) -> {
            ObjectTransferHandle.ElemTransfer temporal = oth.conv(el);
            if (requestImages == null) temporal.setImagesBase64(Collections.emptyList());
            else if (requestImages.matches("\\d+")) Commons.listDeleteRest(temporal.getImagesBase64(), Integer.parseInt(requestImages));
            elems.add(temporal);
        });
        return elems;
    }

    @GetMapping("/genSerial")
    @ResponseBody
    public String generateElemSerial(@RequestParam String compSerial) {
        return elemService.generateSerial(compSerial);
    }

    @GetMapping("/query")
    @ResponseBody
    public List<ElemTransfer> elemQuery(@RequestParam(required = false) String serial,
                                        @RequestParam(required = false) String name,
                                        @RequestParam(required = false) String description,
                                        @RequestParam(required = false) String status,
                                        @RequestParam(required = false) String requestImages) {
        List<Elem> elems = elemService.find(
            Elem.exampleBuider(ExampleMatcher.StringMatcher.CONTAINING).serial(serial)
                                                                       .name(name)
                                                                       .desc(description)
                                                                       .status(status)
                                                                       .build());
        print(serial, name, description, status);
        return elems.stream()
            .map(elem -> oth.conv(elem))
            .peek(elem -> {
                if (requestImages == null) elem.setImagesBase64(Collections.emptyList());
                else if (requestImages.matches("\\d+")) Commons.listDeleteRest(elem.getImagesBase64(), Integer.parseInt(requestImages));
            })
            .collect(Collectors.toList());
    }

    private static class ElemTransferBugfix extends ObjectTransferHandle.ElemTransfer {}
}