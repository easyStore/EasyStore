package hu.smartteam.easystore.controller.web.validators;

import hu.smartteam.easystore.controller.ObjectTransferHandle;
import hu.smartteam.easystore.model.Elem;
import hu.smartteam.easystore.service.CompartmentService;
import hu.smartteam.easystore.service.ElemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class ElementAddRequestValidator implements Validator {

    private final ElemService elemService;
    private final CompartmentService compartmentService;

    @Autowired
    public ElementAddRequestValidator(ElemService elemService, CompartmentService compartmentService) {
        this.elemService = elemService;
        this.compartmentService = compartmentService;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return ObjectTransferHandle.ElemTransfer.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ObjectTransferHandle.ElemTransfer dto = (ObjectTransferHandle.ElemTransfer) target;
        if (elemService.findOne(
            Elem.exampleBuider().compartment(compartmentService.findBySerial(dto.getCompartmentSerial()))
                                .serial(dto.getSerial())
                                .build()) != null) {
            errors.rejectValue("serial", "alreadyExists", "already exists");
        }
    }

}