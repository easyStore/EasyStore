package hu.smartteam.easystore.controller.web;

import hu.smartteam.easystore.model.Tag;
import hu.smartteam.easystore.repositories.TagRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class WebController {

    private final TagRepository tagRepository;

    public WebController(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @PostMapping("/test")
    public ResponseEntity<String> test(@RequestBody String data) {
        System.out.println(data);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }

    @GetMapping("/tag/add")
    public ResponseEntity<String> addTag(@RequestParam String name) {
        try {
            tagRepository.save(new Tag(null, null, name));
            return new ResponseEntity<>("Success", HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>("Unexpected error", HttpStatus.OK);
        }
    }
}