package hu.smartteam.easystore.controller.web;

import hu.smartteam.easystore.controller.ObjectTransferHandle;
import hu.smartteam.easystore.controller.ObjectTransferHandle.CompartmentTransfer;
import hu.smartteam.easystore.controller.web.validators.CompartmentAddRequestValidator;
import hu.smartteam.easystore.model.Compartment;
import hu.smartteam.easystore.model.Elem;
import hu.smartteam.easystore.model.Tag;
import hu.smartteam.easystore.repositories.TagRepository;
import hu.smartteam.easystore.service.CompartmentService;
import hu.smartteam.easystore.service.ElemService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import static hu.smartteam.easystore.common.Utils.print;

@Controller
@RequestMapping("/compartment")
public class CompartmentController {

    private final ObjectTransferHandle oth;
    private final CompartmentService compartmentService;
    private final CompartmentAddRequestValidator saveVal;
    private final TagRepository tagRepository;
    private final ElemService elemService;

    @Autowired
    CompartmentController(ObjectTransferHandle oth,
                          CompartmentService compartmentService,
                          CompartmentAddRequestValidator saveVal,
                          TagRepository tagRepository,
                          ElemService elemService) {
        this.oth = oth;
        this.compartmentService = compartmentService;
        this.saveVal = saveVal;
        this.tagRepository = tagRepository;
        this.elemService = elemService;
    }

    @InitBinder("compartmentTransferBugfix")
    public void setupBinder(WebDataBinder binder) {
        binder.addValidators(saveVal);
    }

    @PostMapping("/add")
    public ResponseEntity<String> addCompartment(@Valid @RequestBody CompartmentTransferBugfix dto) {
        return saveCompartment(dto);
    }

    @PostMapping("/save")
    public ResponseEntity<String> saveCompartment(@Valid @RequestBody CompartmentTransfer dto) {
        try {
            System.out.println(dto);
            Compartment compartment = oth.conv(dto);
            System.out.println(compartment==null?compartment:"null");
            print(compartmentService.save(compartment));

            return Commons.validatedResponseEntity();
        }
        catch(IllegalArgumentException e) {
            return Commons.responseEntity("Error: " + e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        catch(Exception e) {
            e.printStackTrace();
            return Commons.responseEntity("Unexpected error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/delete")
    public ResponseEntity<String> deleteCompartment(@RequestParam String serial) {
        try {
            Compartment deleting = compartmentService.findBySerial(serial);
            elemService.find(Elem.exampleBuider().compartment(deleting).build())
                .forEach(elem -> elemService.delete(elem));
            compartmentService.delete(deleting);
            return Commons.responseEntity("Success", HttpStatus.OK);
        }
        catch(Exception e) {
            e.printStackTrace();
            return Commons.responseEntity("Unexpected error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("")
    @ResponseBody
    public CompartmentTransfer getCompartment(@RequestParam String serial) {
        return oth.conv(compartmentService.findBySerial(serial));
    }

    @GetMapping("/all")
    @ResponseBody
    public List<CompartmentTransfer> getAllCompartment(@RequestParam Integer page,
                                                       @RequestParam Integer pageSize,
                                                       @RequestParam(required = false) String requestImages) {
        List<CompartmentTransfer> compartments = new ArrayList<>(pageSize);
        compartmentService.findAll(page, pageSize).forEach((comp) -> {
            CompartmentTransfer temporal = oth.conv(comp);
            if (requestImages == null) temporal.setImagesBase64(Collections.emptyList());
            else if (requestImages.matches("\\d+")) Commons.listDeleteRest(temporal.getImagesBase64(), Integer.parseInt(requestImages));
            compartments.add(temporal);
        });
        return compartments;
    }

    @GetMapping("/query")
    @ResponseBody
    public List<CompartmentTransfer> compartmentQuery(@RequestParam(required = false) String serial,
                                                      @RequestParam(required = false) String name,
                                                      @RequestParam(required = false) String room,
                                                      @RequestParam(required = false) String shelf,
                                                      @RequestParam(required = false) String tags,
                                                      @RequestParam(required = false) String requestImages) {
        Set<Tag> tagSet = null;
        if (tags!=null) {
            tagSet = Arrays.stream(tags.split(","))
                .map(s -> s.trim())
                .map(s -> tagRepository.findByText(s))
                .collect(Collectors.toSet());
        }
        List<Compartment> compartments = compartmentService.find(
            Compartment.exampleBuilder(ExampleMatcher.StringMatcher.CONTAINING).serial(serial)
                                                                               .name(name)
                                                                               .room(room)
                                                                               .shelf(shelf)
                                                                               .tags(tagSet)
                                                                               .build());
        print("Query:", serial, name, room, shelf, tagSet);
        return compartments.stream()
            .map(comp -> oth.conv(comp))
            .peek(comp -> {
                if (requestImages == null) comp.setImagesBase64(Collections.emptyList());
                else if (requestImages.matches("\\d+")) Commons.listDeleteRest(comp.getImagesBase64(), Integer.parseInt(requestImages));
            })
            .collect(Collectors.toList());
    }

    private static class CompartmentTransferBugfix extends ObjectTransferHandle.CompartmentTransfer {}
}
