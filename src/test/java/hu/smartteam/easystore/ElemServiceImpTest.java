package hu.smartteam.easystore;

import hu.smartteam.easystore.model.Compartment;
import hu.smartteam.easystore.model.Elem;
import hu.smartteam.easystore.repositories.CompartmentRepository;
import hu.smartteam.easystore.repositories.ElemRepository;
import hu.smartteam.easystore.service.CompartmentService;
import hu.smartteam.easystore.service.ElemService;
import hu.smartteam.easystore.service.imp.CompartmentServiceImp;
import hu.smartteam.easystore.service.imp.ElemServiceImp;
import java.util.Collections;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ElemServiceImpTest {
    @Mock
    private CompartmentRepository compartmentRepository;
    @Mock
    private ElemRepository elemRepository;
    private CompartmentService compartmentService;
    private ElemService elemService;

    @Before
    public void setUp() {
        Compartment givenCompartment = Compartment.compartmentBuilder()
            .name("Test comp")
            .pictures(Collections.emptyList())
            .room("Test room")
            .serial("1")
            .shelf("Test shelf")
            .build();
        Mockito.when(compartmentRepository.findOne(Compartment.exampleBuilder().serial("1").build())).thenReturn(givenCompartment);
        compartmentService = new CompartmentServiceImp(compartmentRepository);

        Elem givenElem = Elem.elemBuilder()
            .name("Test elem")
            .pictures(Collections.emptyList())
            .desc("Description")
            .status("Restock")
            .value(0.0)
            .serial("1")
            .build();
        Mockito.when(elemRepository.findOne(Elem.exampleBuider()
            .serial("1")
            .compartment(new Compartment("1", null))
            .build())).thenReturn(Elem.elemBuilder()
                .name("Test elem")
                .pictures(Collections.emptyList())
                .desc("Description")
                .status("Restock")
                .value(0.0)
                .serial("1")
                .compartment(givenCompartment)
                .build());
        Mockito.doAnswer(l -> {Mockito.reset(elemRepository); return null;}).when(elemRepository).delete(givenElem);
        elemService = new ElemServiceImp(elemRepository, compartmentService);
    }

    @Test
    public void whenValidSerial_thenFound() {
        Elem found = elemService.findBySerial("1", "1");
        Assertions.assertThat(found.getName()).isEqualTo("Test elem");
    }

    @Test
    public void whenDelete_thenElemDeletedAndCompartmentNotTouched() {
        Elem foundElem = elemService.findBySerial("1", "1");
        Assertions.assertThat(foundElem.getName()).isEqualTo("Test elem");
        elemService.delete(foundElem);
        foundElem = elemService.findBySerial("1", "1");
        Assertions.assertThat(foundElem).isNull();

        Compartment foundCompartment = compartmentService.findBySerial("1");
        Assertions.assertThat(foundCompartment.getName()).isEqualTo("Test comp");
    }
}