package hu.smartteam.easystore;

import hu.smartteam.easystore.model.Compartment;
import hu.smartteam.easystore.repositories.CompartmentRepository;
import hu.smartteam.easystore.service.CompartmentService;
import hu.smartteam.easystore.service.imp.CompartmentServiceImp;
import java.util.Collections;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CompartmentServiceImpTest {

    @Mock
    private CompartmentRepository compartmentRepository;
    private CompartmentService compartmentService;

    @Before
    public void setUp(){
        Compartment given = Compartment.compartmentBuilder()
            .name("Test comp")
            .pictures(Collections.emptyList())
            .room("Test room")
            .serial("1")
            .shelf("Test shelf")
            .build();
        Mockito.when(compartmentRepository.findOne(Compartment.exampleBuilder().serial("1").build())).thenReturn(given);
        compartmentService = new CompartmentServiceImp(compartmentRepository);
    }

    @Test
    public void whenValidSerial_thenFound() {
        Compartment found = compartmentService.findBySerial("1");
        Assertions.assertThat(found.getName()).isEqualTo("Test comp");
    }
}