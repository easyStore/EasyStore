package hu.smartteam.easystore.integration;

import hu.smartteam.easystore.EasyStoreApplication;
import hu.smartteam.easystore.common.Utils;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = EasyStoreApplication.class)
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CompartmentControllerTest {

    @Autowired private MockMvc mvc;

    @Test
    public void no1_addCompartment_statusOK() throws Exception {
        mvc.perform(post("/compartment/add")
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .content("{\n" +
                        "\"serial\": \"CompartmentControllerTest\",\n" +
                        "\"name\": \"Test name\",\n" +
                        "\"room\": \"Test room\",\n" +
                        "\"shelf\": \"Test shelf\",\n" +
                        "\"imagesBase64\": [ ],\n" +
                        "\"infos\": { },\n" +
                        "\"tags\": [\"test\"]\n" +
                    "}")
            ).andDo(result -> Utils.print("\n\n\n\nRESULT", result.getResponse().getContentAsString(), "\n\n\n\n"))
            .andExpect(status().isOk())
            .andExpect(content().json("[\"Success\"]"));
    }

    @Test
    public void no2_getCompartment() throws Exception {
        mvc.perform(get("/compartment?serial=CompartmentControllerTest"))
            .andDo(result -> Utils.print("\n\n\n\nRESULT", result.getResponse().getContentAsString(), "\n\n\n\n"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.name", is("Test name")));
    }

    @Test
    public void no3_deleteCompartment_statusOK() throws Exception {
        mvc.perform(get("/compartment/delete?serial=CompartmentControllerTest"))
            .andDo(result -> Utils.print("\n\n\n\nRESULT", result.getResponse().getContentAsString(), "\n\n\n\n"))
            .andExpect(status().isOk())
            .andExpect(content().json("[\"Success\"]"));
    }
}