package hu.smartteam.easystore;

import hu.smartteam.easystore.model.Compartment;
import hu.smartteam.easystore.repositories.CompartmentRepository;
import java.util.Collections;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CompartmentDataTest {

    @Autowired private CompartmentRepository repository;
    @Autowired private TestEntityManager entityManager;

    @Test
    public void whenFindOne_thenReturnOne() {

        //Given
        Compartment given = Compartment.compartmentBuilder()
            .name("Test comp")
            .pictures(Collections.emptyList())
            .room("Test room")
            .serial("1")
            .shelf("Test shelf")
            .build();
        Compartment persisted = entityManager.persistAndFlush(given);

        //When
        Compartment found = repository.findOne(persisted.getId());

        //Then
        Assertions.assertThat(found.getName()).isEqualTo(given.getName());
    }
}